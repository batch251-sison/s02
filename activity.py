var = input("Please input a year:")

if var.isnumeric():
    year = int(var)

    if year < 1:
        print("The input cannot be negative or 0")
    else:
        if (year % 4 == 0 and year % 100 != 0) or year % 400 == 0:
            print(f"{year} is a leap year")
        elif year % 100 == 0:
            print(f"{year} is not a leap year")
        else:
            print(f"{year} is not a leap year")
else:
    print("Please input must be a number")


row = int(input("Please input a row: "))
col = int(input("Please input a column: "))

if row < 1 or col < 1:
    print("Row or Column cannot be less than 1")
else:
    while row > 0:
        row -= 1
        print('*' * col)